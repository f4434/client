package com.devups.client.repository;

import com.devups.client.model.Orden;
import org.springframework.data.repository.CrudRepository;

public interface OrdenOneRepository extends CrudRepository<Orden, Long>{
    
}
