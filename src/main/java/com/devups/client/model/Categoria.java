package com.devups.client.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "categoria")
public class Categoria {
    
    @Id
    @SequenceGenerator(
            name = "sq_category",
            sequenceName = "sq_category",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "sq_category",
            strategy = GenerationType.SEQUENCE
    )
    private Long id;
    private String name;
    private String identificador;
    private String empresa;
}
