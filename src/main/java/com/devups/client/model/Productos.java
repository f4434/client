package com.devups.client.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "productos")
public class Productos {
    
    @Id
    @SequenceGenerator(
            name = "sq_products",
            sequenceName = "sq_products",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "sq_products",
            strategy = GenerationType.SEQUENCE
    )
    private Long id;
    private String name;
    private String descripcion;
    private Integer monto;
    private String identificador;
    private String empresa;
    
}
