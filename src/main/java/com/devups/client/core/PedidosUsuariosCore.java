package com.devups.client.core;

import java.util.List;

import com.devups.client.model.dto.PedidosUsuarios;
import com.devups.client.model.entity.PedidosUsuariosEntity;


public interface PedidosUsuariosCore {
    List<PedidosUsuariosEntity> findAll();
    List<PedidosUsuariosEntity> create(PedidosUsuarios entity);
    List<PedidosUsuariosEntity> update(List<PedidosUsuariosEntity> entity);
    Boolean deletePedidos(String id);
    List<PedidosUsuariosEntity> findByNameAndMesa(String name, String mesa);
    List<PedidosUsuariosEntity> findByMesa(String id);
}
