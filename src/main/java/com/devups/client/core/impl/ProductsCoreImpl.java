package com.devups.client.core.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.devups.client.core.ProductsCore;
import com.devups.client.model.Productos;
import com.devups.client.repository.ProductsOneRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

@Service
public class ProductsCoreImpl implements ProductsCore {

    @Autowired
    ProductsOneRepository repo;

    @Override
    public List<Productos> findAll() {
        return Streamable.of(repo.findAll()).toList();
    }

    @Override
    public List<Productos> create(List<Productos> entity) {
        return entity.stream().map(repo::save).collect(Collectors.toList());
    }

    @Override
    public Productos update(Productos entity) {
        return repo.save(entity);
    }

    @Override
    public Boolean delete(Long id) {
        repo.deleteById(id);
        return true;
    }

    @Override
    public List<Productos> findAllById(String id) {
        return Streamable.of(repo.findAll()).toList()
        .stream()
        .filter(element -> element.getIdentificador().equals(id))
        .collect(Collectors.toList());
    }







    
}
