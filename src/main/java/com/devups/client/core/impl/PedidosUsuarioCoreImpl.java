package com.devups.client.core.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.devups.client.core.PedidosUsuariosCore;
import com.devups.client.model.dto.PedidosUsuarios;
import com.devups.client.model.dto.SolicitudUsuario;
import com.devups.client.model.entity.PedidosUsuariosEntity;
import com.devups.client.repository.PedidosUsuariosRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

@Service
public class PedidosUsuarioCoreImpl implements PedidosUsuariosCore{

    @Autowired
    PedidosUsuariosRepository repository;

    @Override
    public List<PedidosUsuariosEntity> findAll() {
        return Streamable.of(repository.findAll()).toList();
    }

    @Override
    public List<PedidosUsuariosEntity> create(PedidosUsuarios dto) {
        List<PedidosUsuariosEntity> entities = new ArrayList<>();
        for (SolicitudUsuario solicitudUsuario : dto.getSolicitudUser()) {
            PedidosUsuariosEntity entity = new PedidosUsuariosEntity();
            entity.setComentario(solicitudUsuario.getComentario());
            entity.setEmpresa(dto.getEmpresa());
            entity.setId(null);
            entity.setName(dto.getName());
            entity.setNumeroMesa(dto.getNumeroMesa());
            entity.setPlatillo(solicitudUsuario.getPlatillo());
            entity.setPrecio(solicitudUsuario.getPrecio());

            entities.add(repository.save(entity));
        }
        return entities;
    }

    @Override
    public List<PedidosUsuariosEntity> update(List<PedidosUsuariosEntity> dto) {
        List<PedidosUsuariosEntity> entities = new ArrayList<>();
        for (PedidosUsuariosEntity solicitudUsuario : dto) {
            entities.add(repository.save(solicitudUsuario));
        }
        return entities;
    }

    @Override
    @Transactional
    public Boolean deletePedidos(String id) {
        repository.deleteByNumeroMesa(id);
        return true;
    }

    @Override
    public List<PedidosUsuariosEntity> findByNameAndMesa(String name, String mesa) {
        return repository.findAllByNumeroMesaAndName(mesa, name);
    }

    @Override
    public List<PedidosUsuariosEntity> findByMesa(String id) {
        return repository.findAllByNumeroMesa(id);
    }
}