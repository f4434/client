package com.devups.client.core;

import java.util.List;

import com.devups.client.model.Productos;

public interface ProductsCore {
    List<Productos> findAll();
    List<Productos> create(List<Productos> ordensEntity);
    Productos update(Productos ordensEntity);
    Boolean delete(Long id);
    List<Productos> findAllById(String id);
}
