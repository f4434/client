package com.devups.client.controller;

import java.util.List;

import com.devups.client.core.MesaOcupadaCore;
import com.devups.client.model.entity.MesaOcupadaEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mesaocupada")
public class MesaOcupadaController {
    
    @Autowired
    MesaOcupadaCore core;
    
    @PostMapping("/crear")
    MesaOcupadaEntity crearOrden(@RequestBody MesaOcupadaEntity ordensEntity){
        return core.create(ordensEntity);

    }

    @PostMapping("/actualizar")
    MesaOcupadaEntity actualizarOrden(@RequestBody MesaOcupadaEntity ordensEntity){
        return core.update(ordensEntity);
    }
    @DeleteMapping("/{id}")
    Boolean borrarOrden(@PathVariable(value = "id") Long id){
        return core.deleteAllMesa(id);
    }

    @GetMapping("/by/{mesa}")
    List<MesaOcupadaEntity> encontrarOrdenNumero(@PathVariable(value = "mesa") String id){
        return core.findById(id);

    }
}
