package com.devups.client.controller;

import java.util.List;

import com.devups.client.core.PedidosUsuariosCore;
import com.devups.client.model.dto.PedidosUsuarios;
import com.devups.client.model.entity.PedidosUsuariosEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/solicitudPedido")
public class PedidosUsuariosController {
    
    @Autowired
    PedidosUsuariosCore core;
    
    @PostMapping("/crear")
    List<PedidosUsuariosEntity> crearOrden(@RequestBody PedidosUsuarios dto){
        return core.create(dto);

    }

    @PostMapping("/actualizar")
    List<PedidosUsuariosEntity> actualizarOrden(@RequestBody List<PedidosUsuariosEntity> dto){
        return core.update(dto);
    }
    @DeleteMapping("/{mesa}")
    Boolean borrarOrden(@PathVariable(value = "mesa") String mesa){
        return core.deletePedidos(mesa);
    }

    @GetMapping("/todo")
    List<PedidosUsuariosEntity> encontrarOrdenNumero(){
        return core.findAll();
    }

    @GetMapping("/by/{mesa}/{name}")
    List<PedidosUsuariosEntity> getByNameAndId(@PathVariable(value = "mesa") String mesa, @PathVariable(value = "name") String name){
        return core.findByNameAndMesa(name, mesa);
    }

    @GetMapping("/by/{mesa}")
    List<PedidosUsuariosEntity> getByMesa(@PathVariable(value = "mesa") String mesa){
        return core.findByMesa(mesa);
    }
}
