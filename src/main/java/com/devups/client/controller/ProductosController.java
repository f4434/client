package com.devups.client.controller;

import java.util.List;
import com.devups.client.core.ProductsCore;
import com.devups.client.model.Productos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/productos")
public class ProductosController {
    
     
    @Autowired
    ProductsCore core;

    @GetMapping("/todo")
    List<Productos> encuentraTodo(){
        return core.findAll();
    }
    
    @PostMapping("/crear")
    List<Productos> crearOrden(@RequestBody List<Productos> ordensEntity){
        return core.create(ordensEntity);

    }
    @PostMapping("/actualizar")
    Productos actualizarOrden(@RequestBody Productos ordensEntity){
        return core.update(ordensEntity);
    }
    @DeleteMapping("/{id}")
    Boolean borrarOrden(@PathVariable(value = "id") Long id){
        return core.delete(id);
    }

    @GetMapping("/by/{id}")
    List<Productos> encontrarOrdenNumero(@PathVariable(value = "id") String id){
        return core.findAllById(id);

    }
}
