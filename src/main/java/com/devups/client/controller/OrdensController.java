package com.devups.client.controller;

import java.util.List;
import java.util.Optional;

import com.devups.client.core.OrdensCore;
import com.devups.client.model.OrdensEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/client")
public class OrdensController {
    
    @Autowired
    OrdensCore ordensCore;

    @GetMapping("/todo")
    List<OrdensEntity> encuentraTodo(){
        return ordensCore.findAll();
    }
    
    @PostMapping("/crear")
    OrdensEntity crearOrden(@RequestBody OrdensEntity ordensEntity){
        return ordensCore.create(ordensEntity);

    }
    @PostMapping("/actualizar")
    Optional<OrdensEntity> actualizarOrden(@RequestBody OrdensEntity ordensEntity){
        return ordensCore.update(ordensEntity);
    }
    @DeleteMapping("/{id}")
    Boolean borrarOrden(@PathVariable(value = "id") Long id){
        return ordensCore.delete(id);
    }

    @GetMapping("/by/{id}")
    OrdensEntity encontrarOrdenNumero(@PathVariable(value = "id") Long id){
        System.out.println(id);
        return ordensCore.findById(id);

    }
}
